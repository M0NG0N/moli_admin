import dayjs from "dayjs";

export default function StringToDate({ stringDate, format }) {
  (format === null || format === undefined || format === "") &&
    (format = "YYYY-MM-DD");
  if (stringDate == null || !stringDate) return null;

  return dayjs(stringDate).format(format);
}
//.toISOString(true).split("T")[0]
