import React, { useState } from "react";
import GoogleMapReact from "google-map-react";

interface LocationSelectorMapProps {
  onLocationSelect: (location: { lat: number; lng: number }) => void;
  defaultCenter: { lat: number; lng: number };
}

const LocationSelectorMap: React.FC<LocationSelectorMapProps> = ({
  onLocationSelect,
  defaultCenter,
}) => {
  const [markerPosition, setMarkerPosition] = useState(defaultCenter);

  const handleMapClick = (event: {
    lat: number;
    lng: number;
    event: React.MouseEvent<HTMLDivElement>;
  }) => {
    const { lat, lng } = event;
    setMarkerPosition({ lat, lng });
    onLocationSelect({ lat, lng });
  };

  return (
    <GoogleMapReact
      defaultCenter={defaultCenter}
      defaultZoom={15}
      onClick={handleMapClick}
    >
      <Marker lat={markerPosition.lat} lng={markerPosition.lng} />
    </GoogleMapReact>
  );
};

const Marker: React.FC<{ lat: number; lng: number }> = () => (
  <div style={{ fontSize: "24px", color: "red" }}>📍</div>
);

const LocationSelector: React.FC<{
  onLocationSelect: (location: { lat: number; lng: number }) => void;
  defaultLocation: { lat: number; lng: number };
}> = ({ onLocationSelect, defaultLocation }) => {
  return (
    <LocationSelectorMap
      onLocationSelect={onLocationSelect}
      defaultCenter={defaultLocation}
    />
  );
};

export default LocationSelector;
