export const breadcrumbnameMap = {
  "/": "Нүүр хуудас",
  "/event/speaker": "Илтгэгч",
  "/event/list": "Эвент жагсаалт",
  "/event": "Эвент",
  "/blog": "Нийтлэл",
  "/blog/list": "Нийтлэл жагсаалт",
  "/advice": "Зөвлөгөө",
  "/advice/list": "Зөвлөгөө жагсаалт",
  "/404": "",
};
