import React, { ReactNode, useState } from "react";
import { HomeOutlined, LoginOutlined } from "@ant-design/icons";
import { Avatar, Breadcrumb, Button, Layout, Menu, theme } from "antd";
import Link from "next/link";
import { signOut } from "next-auth/react";
import { v4 as uuidv4 } from "uuid";


import Logo from "../logo";
import { breadcrumbnameMap } from "./breadcrumb-menu";
import { useRouter } from "next/router";

const { Header, Content, Footer, Sider } = Layout;

// console.log("this is items type" + typeof items);
export default function HomeLayout({
  children,
  menu,
}: {
  children: ReactNode;
  menu: any;
}) {
  const [collapsed, setCollapsed] = useState(false);
  const router = useRouter();
  const url = router.pathname;
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const mainMenus = menu?.filter((menu) => menu.menuType == "MainMenu");
  const subMenus = menu?.filter((menu) => menu.menuType == "SubMenu");
  for (let menu of mainMenus) {
    if (!menu.children) {
      menu.children = []; // Initialize the children array if it doesn't exist
    }
    for (let subMenu of subMenus) {
      if (menu.id == subMenu.parentMenuId) {
        // Check if subMenu doesn't already exist in menu.children
        if (!menu.children.some((child) => child.id == subMenu.id)) {
          menu.children.push(subMenu);
        }
      }
    }
  }
  mainMenus.sort((a, b) => a.viewOrder - b.viewOrder);
  const items = mainMenus.map((e, i) => {
    if (e.children?.length > 0) {
      return {
        key: uuidv4(),
        icon: <LoginOutlined className="text-red-500" />,
        label: <span>{e.name}</span>,
        children: e.children.map((child, i) => {
          return {
            key: uuidv4(),
            icon: e.icon,
            label: <Link href={child.menuUrl}>{child.name}</Link>,
          };
        }),
      };
    }
    return {
      key: uuidv4(),
      icon: <LoginOutlined className="text-red-500" />,
      label: (
        <Link href={e.menuUrl} className="">
          <span className="">{e.name}</span>
        </Link>
      ),
    };
  });

  const extraBreadcrumbItems = (url) => {
    return (
      <Breadcrumb.Item key={url}>
        <Link href={url}>{breadcrumbnameMap[url]}</Link>
      </Breadcrumb.Item>
    );
  };

  const breadcrumbItems = [
    <Breadcrumb.Item key="home">
      <Link href="/" passHref>
        <HomeOutlined />
      </Link>
    </Breadcrumb.Item>,
  ].concat(extraBreadcrumbItems(url));
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div className="demo-logo-vertical" />
        <Logo collapsed={collapsed} />
        {/* <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
          {items.map((item) => (
            <Menu.Item key={item.key} icon={item.icon}>
              <Link href={item.Menulink || ""}>{item.label}</Link>
            </Menu.Item>
          ))}
        </Menu> */}

        <Menu
          theme="dark"
          mode="inline"
          items={items.concat({
            key: 99,
            // danger: true,
            icon: <LoginOutlined className="text-red-500" />,
            label: (
              <a
                className="text-red-500 hover:text-red-600"
                onClick={() => {
                  signOut({ redirect: true, callbackUrl: "/login" });
                  localStorage.setItem("menus", null);
                  localStorage.setItem("count", null);
                }}
              >
                Гарах
              </a>
            ),
          })}
          // onClick={items}
        />
      </Sider>
      <Layout>
        <Header
          style={{ padding: 0, background: colorBgContainer }}
          className="flex justify-end items-center "
        >
          <div className="mr-4">
            <div className="relative ml-7">
              <div className="absolute w-2 h-2 rounded-full bottom-3 right-1 z-50 border-white border bg-green-500/75"></div>
              <Link href={"/user/profile"}>
                <Avatar size={44} src="/profile.png" />
              </Link>
            </div>
          </div>
        </Header>
        <Content style={{ margin: "0 16px" }}>
          <Breadcrumb className={"m-3 ml-6"}>{breadcrumbItems}</Breadcrumb>

          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: colorBgContainer,
            }}
          >
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          MOLI ©2024 MOLI Admin Panel
        </Footer>
      </Layout>
    </Layout>
  );
}
