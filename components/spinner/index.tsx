import { Spin, Modal } from "antd";

export default function Spinner({ open }) {
  return (
    <Modal open={open} closable={false} footer={null}>
      <div className="justify-center">
        <Spin style={{ width: "100%" }} tip="Түр хүлээнэ үү ..." />
      </div>
    </Modal>
  );
}
