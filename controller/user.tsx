import prisma from "@/lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";

export const getUserInfo = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  try {
    const user = await prisma.users.findUnique({
      where: {
        id: parseInt(req.query.id as string),
      },
      select: {
        id: true,
        firstName: true,
        lastName: true,
        bio: true,
        phoneNumber: true,
        email: true,
      },
    });

    return res.json({
      success: true,
      status: 200,
      data: user ?? {},
      message: "",
    });
  } catch (error) {
    console.error(error);

    return res.json({
      success: false,
      status: 500,
      data: {},
      message: "Алдаа",
    });
  }
};

export const saveProfile = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  try {
    const body = req.body.values;
    const id = body.id;
    delete body.id;
    const updateUser = await prisma.users.update({
      where: {
        id: parseInt(id as string),
      },
      data: req.body.values,
    });

    return res.json({
      success: true,
      status: 200,
      data: updateUser ?? {},
      message: "",
    });
  } catch (error) {
    console.log(error);
    return res.json({
      success: false,
      status: 500,
      message: "error",
    });
  }
};
