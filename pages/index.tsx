// Import the getPosts function if it's not already imported.

import React, { useState } from "react";
import { GetServerSidePropsContext, NextPage } from "next";
import HomeLayout from "../components/Layout";
import { EditOutlined } from "@ant-design/icons";

import { getSession } from "next-auth/react";
import { Session } from "next-auth";

import { Button, Col, Form, Input, Modal, Row, Statistic, Table  } from "antd";
import StringToDate from "@/components/stringToDate";
import axios from "axios";
import { Notification } from "@/components/notification";
import { useRouter } from "next/router";
interface HomeProps {
  user: any;
  users:any
}
interface User {
  id: string;
  name?: string | null | undefined;
  email?: string | null | undefined;
  roleId:string
}
const Home: NextPage<HomeProps> = ({ user,users }) => {
  const [content, setContent] = useState("");
  const router = useRouter();

  const [open, setOpen] = useState(false);
  const [tmp, setTmp] = useState(null);
  const columns = [
    {
      title: "№",
      key: "count",
      render: (text, object, index) => index + 1,
    },
    {
      title: 'Овог',
      dataIndex: 'lastName',
      key: 'lastName',
    },
    {
      title: 'Нэр',
      dataIndex: 'firstName',
      key: 'firstName',
    },
    {
      title: 'E-mail',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Утасны дугаар',
      dataIndex: 'phoneNumber',
      key: 'phoneNumber',
    },
    {
      title: 'Үүссэн огноо',
      dataIndex: 'createdDate',
      key: 'createdDate',
      render: (createdDate) => (
        <div>
          <StringToDate stringDate={createdDate} format={"YYYY-MM-DD"} />
        </div>
      ),
    },
    {
      title: 'Засах',
      key: 'edit',
      render: (_,record) => (
        <div>
            <EditOutlined onClick={() => {edit(record)}} color="#319bec" size={17} />
        </div>
      ),
    },
  ];
  const edit = (record) => {
    setTmp(record);
    setOpen(true);
  }
  const [form] = Form.useForm();
  const EditForm = ({ open, onCreate, onCancel }) => {
    if (tmp) form.setFieldsValue(tmp);
    return (
      <Modal
        className="top-1/2 transform -translate-y-1/2 transition-transform duration-300 ease-out bounce "
        open={open}
        title="Худалдааны төлөөлөгчийн бүртгэл"
        closable={false}
        // onCancel={onCancel}
        footer={[
          <Button key={"cancel"} onClick={onCancel}>
            Болих
          </Button>,
          <Button
            key={"confirm"}
            onClick={() => {
              form
                .validateFields()
                .then((values) => {
                  form.resetFields();
                  onCreate(values);
                })
                .catch((info) => {
                  console.log("Validate Failed:", info);
                });
            }}
            type="primary"
          >
            Хадгалах
          </Button>,
        ]}
        onOk={() => {
          form
            .validateFields()
            .then((values) => {
              form.resetFields();
              onCreate(values);
            })
            .catch((info) => {
              console.log("Validate Failed:", info);
            });
        }}
      >
        <Form form={form} layout="vertical">
          <Form.Item
            name="firstName"
            label="Нэр"
            style={{ marginBottom: 10 }}
            rules={[
              {
                required: true,
                message: "Нэр оруулна уу",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="lastName"
            label="Овог"
            style={{ marginBottom: 10 }}
            rules={[
              {
                required: true,
                message: "Овог оруулна уу",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="phoneNumber"
            label="Утас"
            style={{ marginBottom: 10 }}
            rules={[
              {
                required: true,
                message: "Утас заавал оруулна уу!",
              },
              {
                pattern: new RegExp(/^[0-9]+$/),
                message: "Дугаараа зөв оруулна уу",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"email"}
            label="И-мэйл"
            style={{ marginBottom: 10 }}
            rules={[
              {
                required: true,
                message: "И-мейл заавал оруулна уу!",
              },
              {
                type: "email",
                message: "И-мейл ээ зөв оруулна уу!",
              },
            ]}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    );
  };

  const onCreate = (values) => {
    const portalUser = {
      ...values,
      id:tmp.id
    };
    axios
      .post("/api/user/update", {
        ...portalUser,
      })
      .then((res) => {
        if (res.data.success)  {
          setTmp(null);
           Notification(
            "Амжилттай",
            "Хэрэглэгчийн мэдээлэл амжилттай хадгалагдлаа",
            "success"
          );
        } else {
           Notification(
            "Анхааруулга",
            "Алдаа гарлаа" + res.data.message,
            "error"
          );
        }
        router.reload();

      })
      .catch((err) => console.log("err====", err));
    setOpen(false);
  };
  return (
    <HomeLayout menu={user.menu}>
        <Row className="mx-10">
           <Table className="w-full" dataSource={users} columns={columns} />
        </Row>
        <EditForm
          open={open}
          onCreate={onCreate}
          onCancel={() => {
            setOpen(false);
            setTmp(null);
            form.resetFields();
          }}
        />
    </HomeLayout>
  );
};

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session: Session | null = await getSession({ req: context.req });
  if (!session) {
    return {
      redirect: {
        destination: "/login", // Redirect to the login page if not logged in
        permanent: false,
      },
    };
  }

  
  const user = session?.user ?? null;
  
  const tmpUser = session.user as User;
  if (tmpUser.roleId == 'blogWrite') {
    return {
      redirect: {
        destination: "/blog/list",
        permanent: false,
      },
    };
  }
  if (tmpUser.roleId == 'couch') {
    return {
      redirect: {
        destination: "/advice/list",
        permanent: false,
      },
    };
  }
  const users = await prisma.users.findMany();
  const serializedData = users.map((item) => ({
    ...item,
    createdDate: item.createdDate.toISOString(),
    modifiedDate: item.modifiedDate.toISOString(),
  }));
  return {
    props: {
      user: user,
      users:serializedData
    },
  };
};
export default Home;
