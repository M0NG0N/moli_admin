import NextAuth from "next-auth";
import Credentials from "next-auth/providers/credentials";
import { convertToPassword } from "@/utils/convert-password";

import prisma from "../../../lib/prisma";  // adjust the path to your prisma instance


export default NextAuth({
  providers: [
    Credentials({
      id: 'credentials',
      name: 'Credentials',
      credentials: {
        email: { label: "Email", type: "text" },
        password: { label: "Password", type: "password" }
      },
      authorize: async (credentials) => {
        const user = await prisma.users.findUnique({
          where: { email: credentials.email },
        });
        const convertedPassword = convertToPassword(credentials.password)
        if (!user || user.password !== convertedPassword) {
          return null;
        }
        const { password, ...rest } = user;
        const roleIds = user.roleId == 'admin'? ['blogWrite', 'couch', 'admin'] : [user.roleId];
        console.log(roleIds)
        const result = await prisma.menu.findMany({
          where: {
            RoleMenu: {
              some: {
                roleId: {
                  in:roleIds
                }
              }
            },
            isVisible: 1
          },
          select: {
            id: true,
            name: true,
            parentMenuId: true,
            menuType: true,
            menuUrl: true,
            isVisible: true,
            viewOrder: true
          }
        });
        console.log(result)
        return {
          ...rest,
          name: user.firstName,
          id: user.id + '',
          menu: result
        };
      }
    })
  ],
  pages: {
    signIn: "/login",
    error: "/500",
  },
  callbacks: {
    session({ session, token }: any) {
      return {
        ...session,
        user: {
          ...session.user,
          id: token.id + '',
          roleId: token.roleId,
          menu: token.menu
        },
      };
    },
    jwt: ({ token, user }) => {
      if (user) {
        const u = user as unknown as any;
        return {
          ...token,
          id: u.id,
          roleId: u.roleId,
          menu: u.menu
        };
      }
      return token;
    },
  },
  secret: process.env.NEXTAUTH_SECRET,

  // adapter: PrismaAdapter(prisma),
});
