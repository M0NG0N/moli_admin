import prisma from "@/lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handle(req: NextApiRequest, res: NextApiResponse) {
    try {
        const { id, firstName, lastName, phoneNumber, email } = req.body;
        const user = await prisma.users.update({
            where: {
                id: id,
            },
            data: {
                firstName: firstName,
                lastName: lastName,
                phoneNumber: phoneNumber,
                email: email.toLowerCase(),
                modifiedDate: new Date(),
            },
        });

        return res.status(200).send({
            success: true,
            status: 200,
            data: {},
            message: "Амжилттай",
          });
    } catch (error: any) {
        return res.status(500).send({
            success: false,
            status: 500,
            message: error.message,
        });
    }
}
