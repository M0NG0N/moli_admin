import { getUserInfo, saveProfile } from "@/controller/user";
import { getSession } from 'next-auth/react';

import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {


    const requestMethod = req.method;
    switch (requestMethod) {
        case 'POST':
            saveProfile(req, res)
            break
        case 'GET':
            const session = await getSession({ req });
            if (!session) {
                return res.status(200).json({ message: 'Unauthorized', status: 400, success: false });
            }
            getUserInfo(req, res)
            break
        default:
            res.setHeader('Allow', ['GET', 'POST'])
            res.status(200).end(`Method ${requestMethod} Not Allowed`)
    }
}