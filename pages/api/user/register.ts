import prisma from "@/lib/prisma";
import { convertToPassword } from "@/utils/convert-password";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handle(req: NextApiRequest, res: NextApiResponse) {
    try {
        const { email, password, roleId } = (await req.body) as {
            email: string;
            password: string;
            roleId: string
        };
        const hashed_password = convertToPassword(password);
        const user = await prisma.users.create({
            data: {
                email: email.toLowerCase(),
                password: hashed_password,
                roleId: roleId,
                createdDate: new Date(),
                modifiedDate: new Date()
            },
        });

        return res.status(200).send({
            user: {
                email: user.email,
            }
        })


    } catch (error: any) {
        return res.status(500).send({
            status: "error",
            message: error.message,
        })

    }
}
