import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/lib/prisma";

// POST /api/post
// Optional fields in body: content
export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {

  const requestMethod = req.method;
  switch (requestMethod) {
    case 'POST':
      try {
        const id = req.body.form.id
        delete req.body.form.id
        const result = await prisma.blogs.upsert({
          where: {
            id: id ? parseInt(id) : 0
          },
          update: { ...req.body.form, authorId: parseInt(req.body.form.authorId) },
          create: { ...req.body.form, authorId: parseInt(req.body.form.authorId) }
        });
        return res.status(200).json({
          success: true,
          status: 200,
          data: result,
          message: "Амжилттай",
        });
      } catch (error) {
        console.log(error);
        return res.status(200).json({
          success: false,
          status: 200,
          message: "Алдаа",
        });
      }
    default:
      res.setHeader('Allow', ['POST'])
      res.status(200).end(`Method ${requestMethod} Not Allowed`)
  }
}
