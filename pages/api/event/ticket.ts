import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/lib/prisma";

// POST /api/post
// Optional fields in body: content
export default async function handle(
    req: NextApiRequest,
    res: NextApiResponse
) {

    const requestMethod = req.method;
    switch (requestMethod) {
        case 'POST':
            try {
                const id = req.body.id
                const result = await prisma.$queryRaw`
                SELECT a.uid, b.firstName, b.email, b.phoneNumber, c.price * SUM(quantity) as totalPrice, SUM(quantity) as totalQuantity
                FROM EventBook a
                LEFT JOIN Users b ON a.uid = b.id
                LEFT JOIN Events c ON c.id = a.eventId
                WHERE eventId = ${id}
                GROUP BY a.uid,b.firstName, b.email, b.phoneNumber, c.price
              `;
                return res.status(200).json({
                    success: true,
                    status: 200,
                    data: result ?? {},
                    message: "Амжилттай",
                });
            } catch (error) {
                console.log(error);
                return res.status(200).json({
                    success: false,
                    status: 200,
                    message: "Алдаа",
                });
            }
        default:
            res.setHeader('Allow', ['POST'])
            res.status(200).end(`Method ${requestMethod} Not Allowed`)
    }
}
