import { GetServerSidePropsContext, NextPage } from "next";

import React, { useState } from "react";
import { Popconfirm, Space, Table } from "antd";
import HomeLayout from "@/components/Layout";

import { Notification } from "@/components/notification";
import axios from "axios";
import { PrismaClient } from "@prisma/client";
import { Session } from "next-auth";
import { getSession } from "next-auth/react";
import Link from "next/link";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import StringToDate from "@/components/stringToDate";
const prisma = new PrismaClient();

type TProps = {
  user: any;
  blog: any;
};
interface User {
  id: string;
  name?: string | null | undefined;
  email?: string | null | undefined;
  roleId: string
}
const BlogList: NextPage<TProps> = ({ user, blog }) => {
  const [data, setData] = useState(blog);
  const deleteBlog = (id) => {
    axios
      .delete(`/api/blog?id=${id}`)
      .then((res) => {
        if (res.data.success) {
          setData(data.filter((e) => e.id !== id));
          Notification("Амжилттай", "Амжилттай устгалдлаа!!!", "success");
        } else {
          Notification("Анхаар", "Илгээхэд алдаа гарсан!!!", "error");
        }
      })
      .catch((err) => console.log(err));
  };
  const columns = [
    {
      title: "№",
      key: "count",
      render: (text, object, index) => index + 1,
    },
    {
      key: "keyss",
      title: "Нэр",
      dataIndex: "title",
    },
    {
      key: "type",
      title: "Төрөл",
      dataIndex: "type",
      render: (type) => (
        <div>{type === "blog" ? <>Нийтлэл</> : <>Мэргэжил</>}</div>
      ),
    },
    {
      key: "createdDate",
      title: "Үүсгэсэн огноо",
      dataIndex: "createdDate",
      render: (createdDate) => (
        <div>
          <StringToDate stringDate={createdDate} format={"YYYY-MM-DD HH:mm"} />
        </div>
      ),
    },
    {
      key: "modifiedDate",
      title: "Зассан огноо",
      dataIndex: "modifiedDate",
      render: (modifiedDate) => (
        <div>
          <StringToDate stringDate={modifiedDate} format={"YYYY-MM-DD HH:mm"} />
        </div>
      ),
    },
    {
      title: "",
      key: "action",
      render: (text, record) => (
        <Space size="small" align="center">
          {record.vendorRequestId != 4 && (
            <Link
              href={{
                pathname: "/blog",
                query: { id: record.id },
              }}
            >
              <EditOutlined className="text-[#319bec]" size={18} />
            </Link>
          )}
          <Popconfirm
            title="Та блог устгахдаа итгэлтэй байна уу?"
            onConfirm={() => deleteBlog(record.id)}
            okText={"Тийм"}
            cancelText="Үгүй"
          >
            <DeleteOutlined className="text-red-500" size={15} />
          </Popconfirm>
        </Space>
      ),
      width: 100,
    },
  ];
  return (
    <HomeLayout menu={user.menu}>
      <>
        <Table
          // loading={loading}
          bordered
          rowKey={"id"}
          className="myTable"
          locale={{ emptyText: "Өгөгдөл байхгүй байна" }}
          columns={columns}
          dataSource={data}
          pagination={{ size: "small", position: ["bottomCenter"] }}
        />
      </>
    </HomeLayout>
  );
};

export default BlogList;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session: Session | null = await getSession({ req: context.req });
  const tmpUser = session.user as User;
  let blog = []
  if (tmpUser?.roleId === 'admin') {
    // If the user is an admin, fetch all blogs
     blog = await prisma.blogs.findMany({
      select: {
        id: true,
        title: true,
        type: true,
        createdDate: true,
        modifiedDate: true,
      },
    });
  
    // Use the allBlogs data
  } else {
    // If the user is not an admin, fetch blogs based on authorId
     blog = await prisma.blogs.findMany({
      where: {
        authorId: parseInt(tmpUser.id),
      },
      select: {
        id: true,
        title: true,
        type: true,
        createdDate: true,
        modifiedDate: true,
      },
    });
  
    // Use the userBlogs data
  }
  const serializedData = blog.map((item) => ({
    ...item,
    createdDate: item.createdDate.toISOString(),
    modifiedDate: item.modifiedDate.toISOString(),
  }));
  if (!session) {
    return {
      redirect: {
        destination: "/login", // Redirect to the login page if not logged in
        permanent: false,
      },
    };
  }
  const user = session?.user ?? null;

  return {
    props: {
      user: user,
      blog: serializedData,
    },
  };
};
