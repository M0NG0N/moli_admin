import HomeLayout from "@/components/Layout";
import {
  Avatar,
  Button,
  Card,
  Col,
  Descriptions,
  Divider,
  Modal,
  Row,
  Form,
  Input,
} from "antd";
import { Session } from "next-auth";
import { GetServerSidePropsContext } from "next";
import { getSession } from "next-auth/react";
import { useEffect, useState } from "react";
import { EditOutlined } from "@ant-design/icons";
import axios from "axios";
import { Notification } from "@/components/notification";
import Spinner from "@/components/spinner";
interface UserData {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  bio: string;
  phoneNumber: string;
  // Add other properties as needed
}
export default function Profile({ user }) {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<UserData | null>(null);
  const [form] = Form.useForm();

  const getUserInfo = () => {
    setLoading(true);
    axios.get(`/api/user?id=${user.id}`).then((res) => {
      if (res.data.success) {
        setData(res.data.data);
      } else {
        Notification("Алдаа", res.data.message, "error");
      }
      setLoading(false);
    });
  };
  useEffect(() => {
    getUserInfo();
  }, []);

  const openModal = () => {
    setOpen(true);
    form.setFieldsValue(data);
  };
  const saveProfile = () => {
    setOpen(false);
    setLoading(true);
    form
      .validateFields()
      .then((values) => {
        form.resetFields();
        const finalValues = { ...values, id: user.id };
        axios.post(`/api/user`, { values: finalValues }).then((res) => {
          if (res.data.success) {
            setData(res.data.data);
            Notification("", "Амжилттай хадгаллаа", "success");
          } else {
            Notification("Алдаа", res.data.message, "error");
          }
          setLoading(false);
        });
      })
      .catch((info) => {
        console.log("Validate Failed:", info);
      });
  };

  return (
    <HomeLayout menu={user.menu}>
      <div className="max-w-7xl mx-auto">
        <Card title={"Миний мэдээлэл"}>
          <Card>
            <Row>
              <Col span={4}>
                <Avatar size={80} src="/profile.png" />
              </Col>
              <Col span={20}>
                <div className="text-lg font-semibold">
                  {`${data?.firstName} ${data?.lastName}`}
                </div>
                <div className="text-sm font-extralight">Team manager </div>
                <div className="text-sm font-extralight">
                  Leads/United States
                </div>
              </Col>
            </Row>
          </Card>
          <Divider />

          <Descriptions
            title="МЭДЭЭЛЭЛ"
            layout="vertical"
            column={2}
            extra={
              <EditOutlined onClick={() => openModal()} className="text-xl" />
            }
          >
            <Descriptions.Item label="FirstName">
              {data?.firstName}{" "}
            </Descriptions.Item>
            <Descriptions.Item label="LastName">
              {data?.lastName}
            </Descriptions.Item>
            <Descriptions.Item label="Email">{data?.email}</Descriptions.Item>
            <Descriptions.Item label="Phone">
              {data?.phoneNumber}
            </Descriptions.Item>
            <Descriptions.Item label="BIO">{data?.bio}</Descriptions.Item>
          </Descriptions>
        </Card>
      </div>

      <Modal
        className="top-1/2 transform -translate-y-1/2 transition-transform duration-300 ease-out bounce "
        open={open}
        title="Хэрэглэгч"
        closable={false}
        // onCancel={onCancel}
        footer={null}
      >
        <Form form={form} layout="vertical">
          <Form.Item
            name="firstName"
            label="Нэр"
            style={{ marginBottom: 10 }}
            rules={[
              {
                required: true,
                message: "Нэр оруулна уу",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="lastName"
            label="Овог"
            style={{ marginBottom: 10 }}
            rules={[
              {
                required: true,
                message: "Овог оруулна уу",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="phoneNumber"
            label="Утас"
            style={{ marginBottom: 10 }}
            rules={[
              {
                required: true,
                message: "Утас заавал оруулна уу!",
              },
              {
                pattern: new RegExp(/^[0-9]+$/),
                message: "Дугаараа зөв оруулна уу",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"email"}
            label="И-мэйл"
            style={{ marginBottom: 10 }}
            rules={[
              {
                required: true,
                message: "И-мейл заавал оруулна уу!",
              },
              {
                type: "email",
                message: "И-мейл ээ зөв оруулна уу!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item name={"bio"} label="Био" style={{ marginBottom: 10 }}>
            <Input.TextArea rows={4} />
          </Form.Item>

          <Row className="mt-3 flex justify-end">
            <Button
              onClick={() => {
                setOpen(false);
                form.resetFields();
              }}
            >
              Болих
            </Button>

            <Button
              className="bg-green-400 text-white ml-2"
              onClick={() => {
                saveProfile();
              }}
              type="default"
            >
              Хадгалах
            </Button>
          </Row>
        </Form>
      </Modal>
      <Spinner open={loading} />
    </HomeLayout>
  );
}

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session: Session | null = await getSession({ req: context.req });
  if (!session) {
    return {
      redirect: {
        destination: "/login", // Redirect to the login page if not logged in
        permanent: false,
      },
    };
  }
  const user = session?.user ?? null;

  return {
    props: {
      user: user,
    },
  };
};
