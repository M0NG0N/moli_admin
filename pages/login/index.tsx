import { getSession, signIn, SignInOptions } from "next-auth/react";
import { useRouter } from "next/router";
import React from "react";
import { useEffect, useState } from "react";
import {
  InfoCircleOutlined,
  UserOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
  LockOutlined,
} from "@ant-design/icons";
import { Button, Form, Input, Select, Tooltip } from "antd";
import Link from "next/link";
import { Notification } from "@/components/notification";
import axios from "axios";

const LoginScreen: React.FC = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [islogin, setIsLogin] = useState(true);
  const [role, setRole] = useState("default");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [rePass, setRePass] = useState("");

  const handleChange = (value: string) => {
    setRole(value);
  };
  useEffect(() => {
    setLoading(false);
  }, [router]);

  // Call this function on a button click or on component mount to test

  const login = async () => {
    setError("");
    setLoading(true);
    const options: SignInOptions = {
      email: username,
      password: password,
      redirect: false,
      callbackUrl: "/",
    };
    const result = await signIn("credentials", options);
    if (result.error) {
      Notification(
        "",
        "Хэрэглэгчийн нэвтрэх нэр, нууц үг буруу байна.",
        "warning"
      );
      // setError("Хэрэглэгчийн нэвтрэх нэр, нууц үг буруу байна.");
      setLoading(false);
    } else if (result?.status === 200) {
      router.push(result.url);
    }
  };

  const register = async () => {
    if (!email) return Notification("", "Имейл хаягаа оруулна уу", "warning");
    if (pass !== rePass) {
      return Notification("", "Нууц үг хоорондоо таарахгүй байна.", "warning");
    }
    if (role == "default")
      return Notification("", "Хэрэглэгчийн төрлөө сонгоно уу", "warning");
    setLoading(true);
    const values = {
      email,
      password: pass,
      roleId: role,
    };
    axios
      .post("/api/user/register", {
        ...values,
      })
      .then(({ data }) => {
        setUsername(data.user.email);
        setIsLogin(true);
        Notification("Амжилттай", "Амжилттай хадгаллаа!!!", "success");
      })
      .catch((error) => {
        console.log(error.message);
        Notification("Анхаар", "Илгээхэд алдаа гарсан!!!", "error");
      });
  };

  return (
    <>
      <img src="/wave.png" className="fixed hidden lg:block inset-0 h-full" />
      <div className="w-screen h-screen flex flex-col justify-center items-center lg:grid lg:grid-cols-2">
        <img
          src="/unlock.svg"
          className="hidden lg:block w-1/2 hover:scale-150 transition-all duration-500 transform mx-auto"
        />
        {islogin ? (
          <Form className="flex flex-col justify-center items-center w-3/4">
            <img src="/moli.png" className="w-52" />
            <h2 className="my-8 font-display font-bold text-3xl text-gray-700 text-center">
              Moli веб сайтанд тавтай морил
            </h2>
            <div className="relative mt-4">
              <Input
                size="large"
                value={username}
                placeholder="Нэвтрэх нэр"
                onChange={(e) => setUsername(e.target.value)}
                prefix={<UserOutlined className="site-form-item-icon" />}
                suffix={
                  <Tooltip title="Бүртгүүлсэн имейл хаяг">
                    <InfoCircleOutlined style={{ color: "rgba(0,0,0,.45)" }} />
                  </Tooltip>
                }
              />
            </div>
            <div className="relative mt-4 ">
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                size="large"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="нууц үг"
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
                onPressEnter={() => login()}
              />
            </div>
            <Button
              type="primary"
              className="  mt-8 px-20 bg-primarycolor rounded-full bg-blue-500 text-white font-bold uppercase  transform hover:translate-y-1 transition-all duration-500"
              onClick={() => login()}
            >
              Нэвтрэх
            </Button>

            <div className=" mt-4 relative">
              <div className="absolute inset-0 flex items-center">
                <div className="w-full border-t border-gray-500" />
              </div>
              <div className="relative flex justify-center text-sm">
                <span className="px-2 bg-white text-gray-500">Эсвэл</span>
              </div>
            </div>

            <Button
              onClick={() => {
                setIsLogin(false);
                setEmail("");
                setPass("");
                setRePass("");
                setRole("default");
              }}
              type="primary"
              className="  mt-4 px-[4.3rem] bg-primarycolor rounded-full bg-blue-500 text-white font-bold uppercase  transform hover:translate-y-1 transition-all duration-500"
            >
              Бүртгүүлэх
            </Button>

            <Link href="#" className="self-end mt-4 text-gray-600 font-bold">
              Нууц үг мартсан?
            </Link>
          </Form>
        ) : (
          <Form className="flex flex-col justify-center items-center w-3/4">
            <img src="/moli.png" className="w-52" />
            <h2 className="my-8 font-display font-bold text-3xl text-gray-700 text-center">
              Moli веб сайтанд тавтай морил
            </h2>
            <div className="relative mt-4">
              <Input
                size="large"
                placeholder="Имейл хаяг"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                prefix={<UserOutlined className="site-form-item-icon" />}
                suffix={
                  <Tooltip title="Бүртгүүлэх имейл хаяг">
                    <InfoCircleOutlined style={{ color: "rgba(0,0,0,.45)" }} />
                  </Tooltip>
                }
              />
            </div>
            <div className="relative mt-4 ">
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                size="large"
                value={pass}
                placeholder="Нууц үг"
                onChange={(e) => setPass(e.target.value)}
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
              />
            </div>
            <div className="relative mt-4 ">
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                size="large"
                value={rePass}
                placeholder="Давтан нууц үг"
                onChange={(e) => setRePass(e.target.value)}
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
              />
            </div>
            <Select
              className="mt-4"
              value={role}
              style={{ width: 240 }}
              onChange={handleChange}
              options={[
                { value: "default", label: "Хэрэглчийн төрөл сонгох" },
                { value: "couch", label: "Зөвөлгөө өгөх" },
                { value: "blogWrite", label: "Блог бичих" },
                { value: "simple", label: "Энгийн хэрэглэгч" },
              ]}
            />
            <Button
              onClick={() => register()}
              type="primary"
              className="  mt-4 px-20 bg-primarycolor rounded-full bg-blue-500 text-white font-bold uppercase  transform hover:translate-y-1 transition-all duration-500"
            >
              Бүртгүүлэх
            </Button>
            <div className=" mt-4 relative">
              <div className="absolute inset-0 flex items-center">
                <div className="w-full border-t border-gray-500" />
              </div>
              <div className="relative flex justify-center text-sm">
                <span className="px-2 bg-white text-gray-500">Эсвэл</span>
              </div>
            </div>

            <Button
              onClick={() => {
                setIsLogin(true);
                setUsername("");
                setPassword("");
              }}
              type="primary"
              className="  mt-4 px-[4.3rem] bg-primarycolor rounded-full bg-blue-500 text-white font-bold uppercase  transform hover:translate-y-1 transition-all duration-500"
            >
              Нэвтрэх
            </Button>
          </Form>
        )}
      </div>
    </>
  );
};

export async function getServerSideProps(context: any) {
  const session = await getSession(context);
  if (session) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
}

export default LoginScreen;
