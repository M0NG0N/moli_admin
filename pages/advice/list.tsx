import { GetServerSidePropsContext, NextPage } from "next";

import React, { useState } from "react";
import { Modal, Space, Table } from "antd";
import HomeLayout from "@/components/Layout";
import { Notification } from "@/components/notification";
import axios from "axios";
import { Session } from "next-auth";
import { getSession } from "next-auth/react";
import { EditOutlined, EyeOutlined } from "@ant-design/icons";
import Link from "next/link";
import StringToDate from "@/components/stringToDate";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

type TProps = {
  user: any;
  advice: any;
};
interface User {
  id: string;
  name?: string | null | undefined;
  email?: string | null | undefined;
  roleId : string
}
const AdviceList: NextPage<TProps> = ({ user, advice }) => {
  const [data, setData] = useState(advice);
  const [booked, setBooked] = useState([]);
  const [open, setOpen] = useState(false);
  const getByUser = (id) => {
    axios
      .post(`/api/advice/order`, { id })
      .then((res) => {
        console.log(res.data);
        if (res.data.success) {
          setBooked(res.data.data);
          setOpen(true);
          // Notification("Амжилттай", "Амжилттай устгалдлаа!!!", "success");
        } else {
          Notification("Анхаар", "Илгээхэд алдаа гарсан!!!", "error");
        }
      })
      .catch((err) => console.log(err));
  };
  const columns = [
    {
      title: "№",
      key: "count",
      render: (text, object, index) => index + 1,
      width: 40,
    },
    {
      key: "keyss",
      title: "Нэр",
      dataIndex: "title",
    },
    {
      key: "totalSlot",
      title: "Үнэ",
      dataIndex: "price",
    },
    {
      key: "beginDate",
      title: "Эхлэх огноо",
      dataIndex: "beginDate",
      render: (beginDate) => (
        <div>
          <StringToDate stringDate={beginDate} format={"YYYY-MM-DD"} />
        </div>
      ),
    },
    {
      key: "beginDate",
      title: "Эхлэх цаг",
      dataIndex: "startTime",
    },
    {
      key: "beginDate",
      title: "Дуусах цаг",
      dataIndex: "endTime",
    },
    {
      key: "createdDate",
      title: "Үүсгэсэн огноо",
      dataIndex: "createdDate",
      render: (createdDate) => (
        <div>
          <StringToDate stringDate={createdDate} format={"YYYY-MM-DD"} />
        </div>
      ),
    },
    {
      key: "modifiedDate",
      title: "Зассан огноо",
      dataIndex: "modifiedDate",
      render: (modifiedDate) => (
        <div>
          <StringToDate stringDate={modifiedDate} format={"YYYY-MM-DD"} />
        </div>
      ),
    },
    {
      key: "eye",
      title: "",
      render: (modifiedDate, record) => (
        <div className="flex justify-center">
          <EyeOutlined
            onClick={() => getByUser(record.id)}
            className="mx-auto text-lg text-[#319bec]"
          />
        </div>
      ),
    },
    {
      title: "",
      key: "action",
      render: (text, record) => (
        <Space size="small" align="center">
          {record.vendorRequestId != 4 && (
            <Link
              href={{
                pathname: "/advice",
                query: { id: record.id },
              }}
            >
              <EditOutlined className="text-[#319bec]" size={18} />
            </Link>
          )}
          {/* <Popconfirm
            title="Та блог устгахдаа итгэлтэй байна уу?"
            onConfirm={() => {
              deleteAdvice(record.id);
            }}
            okText={"Тийм"}
            cancelText="Үгүй"
          >
            <DeleteOutlined className="text-red-500" size={15} />
          </Popconfirm> */}
        </Space>
      ),
      width: 100,
    },
  ];
  const columns2 = [
    {
      title: "№",
      key: "count",
      render: (text, object, index) => index + 1,
    },
    {
      key: "firstName",
      title: "Нэр",
      dataIndex: "firstName",
    },
    {
      key: "email",
      title: "Email",
      dataIndex: "email",
    },
    {
      key: "time",
      title: "Цаг",
      dataIndex: "time",
      render: (time) => {
        const startTime = `${time}:00`;
        const endTime = `${time + 1}:00`;
        const label = `${startTime} - ${endTime}`;
        return <>{label}</>;
      },
    },
    {
      key: "price",
      title: "Үнэ",
      dataIndex: "price",
    },
  ];
  return (
    <HomeLayout menu={user.menu}>
      <Table
        // loading={loading}
        bordered
        rowKey={"id"}
        className="myTable"
        locale={{ emptyText: "Өгөгдөл байхгүй байна" }}
        columns={columns}
        dataSource={data}
        pagination={{ size: "small", position: ["bottomCenter"] }}
      />
      <Modal
        open={open}
        title="Цаг"
        // closable={false}
        onCancel={() => setOpen(false)}
        footer={null}
      >
        <Table
          rowKey={"uid"}
          columns={columns2}
          dataSource={booked}
          pagination={{ size: "small", position: ["bottomCenter"] }}
        />
      </Modal>
    </HomeLayout>
  );
};

export default AdviceList;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session: Session | null = await getSession({ req: context.req });

  if (!session) {
    return {
      redirect: {
        destination: "/login", // Redirect to the login page if not logged in
        permanent: false,
      },
    };
  }
  const tmpUser = session.user as User;
  const isAdmin = tmpUser.roleId === 'admin'; // Assuming user.role indicates the user's role

  let advice;
  if (isAdmin) {
    // Fetch all advices if user is admin
    advice = await prisma.advice.findMany({
      select: {
        id: true,
        title: true,
        price: true,
        startTime: true,
        endTime: true,
        beginDate: true,
        createdDate: true,
        modifiedDate: true,
      },
    });
  } else {
    // Fetch advices for regular users
    advice = await prisma.advice.findMany({
      where: {
        uid: parseInt(tmpUser?.id),
      },
      select: {
        id: true,
        title: true,
        price: true,
        startTime: true,
        endTime: true,
        beginDate: true,
        createdDate: true,
        modifiedDate: true,
      },
    });
  }
  const serializedData = advice.map((item) => ({
    ...item,
    createdDate: item.createdDate.toISOString(),
    modifiedDate: item.modifiedDate.toISOString(),
    beginDate: item.beginDate.toISOString(),
  }));
  const user = session?.user ?? null;

  return {
    props: {
      user: user,
      advice: serializedData,
    },
  };
};
