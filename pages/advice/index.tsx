import { GetServerSidePropsContext, NextPage } from "next";

import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  Col,
  DatePicker,
  Form,
  Input,
  Row,
  Select,
  Tabs,
  Tag,
  TimePicker,
} from "antd";
import HomeLayout from "@/components/Layout";
import { Notification } from "@/components/notification";
import axios from "axios";
import { slugify } from "@/utils/slugify";
import dayjs, { Dayjs } from "dayjs";
import { CategoryType } from "@/utils/types";
import { PrismaClient } from "@prisma/client";
import { Session } from "next-auth";
import { getSession } from "next-auth/react";
import { useRouter } from "next/router";

type TProps = {
  user: any;
};

const Advice: NextPage<TProps> = ({ user }) => {
  const router = useRouter();
  const [createdDate, setCreatedDate] = useState(dayjs());

  const { id } = router.query;
  const [form] = Form.useForm();
  useEffect(() => {
    if (id !== undefined) {
      getAdvice(id);
    }
  }, []);
  const getAdvice = (id) => {
    axios
      .post("/api/advice", { id: id })
      .then(({ data }) => {
        if (data.success) {
          setCreatedDate(data.data.createdDate);
          const tmp = data.data;
          tmp.beginDate = dayjs(tmp.startDate);
          tmp.startTime = dayjs(tmp.startTime, "HH");
          tmp.endTime = dayjs(tmp.endTime, "HH");
          form.setFieldsValue(tmp);
        } else {
          Notification("Анхаар", "", "error");
        }
      })
      .catch((error) => console.log(error));
  };
  const disabledTime = (current: Dayjs): any => {
    const disabledHoursArray: number[] = [];
    for (let i = 0; i <= 23; i++) {
      if (i < 9 || i > 20) disabledHoursArray.push(i);
    }
    return {
      disabledHours: () => disabledHoursArray,
    };
  };
  function onSubmit(values: any) {
    const finalValues = {
      ...values,
      image: values.image,
      slug: slugify(values.title),
      uid: user.id,
      startTime: values["startTime"].format("HH"),
      endTime: values["endTime"].format("HH"),
      createdDate: createdDate,
      modifiedDate: dayjs(),
      id: id,
    };
    axios
      .post("/api/advice/create", {
        form: finalValues,
      })
      .then(({ data }) => {
        if (data.success) {
          router.push("/advice/list");
          Notification("Амжилттай", "Амжилттай хадгаллаа!!!", "success");
        } else {
          Notification("Анхаар", "Илгээхэд алдаа гарсан!!!", "error");
        }
      })
      .catch((error) => {
        console.log(error.message);
        Notification("Анхаар", "Илгээхэд алдаа гарсан!!!", "error");
      });
  }

  return (
    <HomeLayout menu={user.menu}>
      <div className="mx-auto max-w-7xl">
        <Form
          form={form}
          onFinish={onSubmit}
          // labelCol={{ span: 4 }}
          // wrapperCol={{ span: 25 }}
          layout="vertical"
          size="middle"
          style={{ maxWidth: 2000 }}
        >
          <Card bordered className="mt-3" bodyStyle={{ padding: "0" }}>
            <h1 className="font-semibold text-base flex justify-center mt-5">
              Зөвлөгөө
            </h1>
            <Row>
              <Col lg={14} xs={24} className=" py-4 px-5">
                <div className="relative">
                  <Form.Item
                    style={{ marginBottom: "8px" }}
                    label="Нэр"
                    name="title"
                    rules={[
                      {
                        required: true,
                        message: "Нэр заавал оруулна уу!",
                      },
                    ]}
                    className="w-full"
                  >
                    <Input />
                  </Form.Item>
                </div>

                <Row className="" gutter={30}>
                  <Col sm={12} xs={24}>
                    <Form.Item
                      style={{ marginBottom: "8px" }}
                      label="Эхлэх өдөр"
                      name="beginDate"
                      rules={[
                        {
                          required: true,
                          message: "Эхлэх өдөр сонгоно уу",
                        },
                      ]}
                    >
                      <DatePicker
                        disabled={id != undefined}
                        className="w-full"
                      />
                    </Form.Item>
                  </Col>
                </Row>

                <Row gutter={30} className="">
                  <Col xs={24} sm={12}>
                    <Form.Item
                      label="Эхлэх цаг"
                      name="startTime"
                      rules={[
                        {
                          required: true,
                          message: "Эхлэх цаг оруулна уу",
                        },
                      ]}
                      style={{ marginBottom: "8px" }}
                    >
                      <TimePicker
                        disabled={id != undefined}
                        className="w-full"
                        format={"HH"}
                        disabledTime={disabledTime}
                        defaultValue={dayjs().hour(9).minute(0)}
                        hourStep={1}
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12}>
                    <Form.Item
                      label="Дуусах цаг"
                      name="endTime"
                      style={{ marginBottom: "8px" }}
                      rules={[
                        {
                          required: true,
                          message: "Дуусах цаг оруулна уу",
                        },
                      ]}
                    >
                      <TimePicker
                        disabled={id != undefined}
                        className="w-full"
                        format={"HH"}
                        hourStep={1}
                        defaultValue={dayjs().hour(9).minute(0)}
                        disabledTime={disabledTime}
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col
                lg={10}
                xs={24}
                className={"py-4 px-5 border-l-0 lg:border-l-[1px]"}
              >
                <Form.Item
                  style={{ marginBottom: "8px" }}
                  label="Зураг"
                  name={"image"}
                  rules={[
                    {
                      required: true,
                      message: "Зураг оруулна уу",
                    },
                  ]}
                >
                  <Input placeholder="Зургын линк" type="text" />
                </Form.Item>
                <Form.Item
                  style={{ marginBottom: "8px" }}
                  label="Үнэ"
                  name="price"
                  rules={[
                    {
                      required: true,
                      message: "Үнэ оруулна уу",
                    },
                  ]}
                >
                  <Input type="number" placeholder="Үнэ:" />
                </Form.Item>
                <Form.Item
                  label="Дэлгэрэнгүй мэдээлэл"
                  name="description"
                  style={{ marginBottom: "8px" }}
                  rules={[
                    {
                      required: true,
                      message: "Дэлгэрэнгүй мэдээлэл оруулна уу.",
                    },
                  ]}
                >
                  <Input.TextArea
                    className="w-full mt-2"
                    autoSize={{
                      minRows: 4,
                      maxRows: 6,
                    }}
                    // style={{ width: 350 }}
                    rows={4}
                    placeholder="Дэлгэрэнгүй мэдээлэл:"
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Form>
        <Button
          className="mt-5"
          type="primary"
          onClick={() => {
            form.submit();
          }}
        >
          Хадгалах
        </Button>
      </div>
    </HomeLayout>
  );
};

export default Advice;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session: Session | null = await getSession({ req: context.req });

  if (!session) {
    return {
      redirect: {
        destination: "/login", // Redirect to the login page if not logged in
        permanent: false,
      },
    };
  }
  const user = session?.user ?? null;

  return {
    props: {
      user: user,
    },
  };
};
