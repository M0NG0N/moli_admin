import { GetServerSidePropsContext, NextPage } from "next";
import {
  Input,
  Card,
  Checkbox,
  Button,
  Form,
  Select,
  Row,
  Col,
  DatePicker,
  Tooltip,
  TimePicker,
} from "antd";
import { useState, useEffect } from "react";
import HomeLayout from "@/components/Layout";
import Editor from "@/components/common/Editor";
import { Notification } from "@/components/notification";
import axios from "axios";
import { slugify } from "@/utils/slugify";
import dayjs from "dayjs";
import { PrismaClient } from "@prisma/client";
import { Session } from "next-auth";
import { getSession } from "next-auth/react";
import LocationSelector from "@/components/locationSelector";
import { useRouter } from "next/router";
const prisma = new PrismaClient();

type TProps = {
  user: any;
};

const CreateEvent: NextPage<TProps> = ({ user }) => {
  const router = useRouter();

  const { id } = router.query;

  const [type, setType] = useState("");
  const [createdDate, setCreatedDate] = useState(dayjs());

  const [selectedLocation, setSelectedLocation] = useState(null);

  useEffect(() => {
    if (id !== undefined) {
      getEvent(id);
    }
  }, []);
  const getEvent = (id) => {
    axios.post("/api/event", { id: id }).then(({ data }) => {
      if (data.success) {
        console.log(data);
        setCreatedDate(data.data.createdDate);
        const tmp = data.data;
        tmp.startDate = dayjs(tmp.startDate);
        tmp.endDate = dayjs(tmp.endDate);
        tmp.startTime = dayjs(tmp.startTime, "HH:mm");
        tmp.endTime = dayjs(tmp.endTime, "HH:mm");
        form.setFieldsValue(tmp);
      } else {
        Notification("Анхаар", "", "error");
      }
    });
  };
  const handleLocationSelect = (location: {
    lat: number;
    lng: number;
  }): void => {
    console.log(
      `Selected location - Latitude: ${location.lat}, Longitude: ${location.lng}`
    );
  };
  const [form] = Form.useForm();

  function onSubmit(values: any) {
    console.log(values);
    const finalValues = {
      ...values,
      image: values.image,
      slug: slugify(values.title),
      authorId: user.id,
      city: "Улаанбаатар",
      country: "Монгол улс",
      startTime: values["startTime"].format("HH:mm:ss"),
      endTime: values["endTime"].format("HH:mm:ss"),
      createdDate: createdDate,
      modifiedDate: dayjs(),
      latitude: 47.92,
      longitude: 106.91,
      id: id,
    };
    axios
      .post("/api/event/create", {
        form: finalValues,
      })
      .then(({ data }) => {
        router.push("/event/list");

        Notification("Амжилттай", "Амжилттай хадгаллаа!!!", "success");
      })
      .catch((error) => {
        console.log(error.message);
        Notification("Анхаар", "Илгээхэд алдаа гарсан!!!", "error");
      });
  }

  return (
    <HomeLayout menu={user.menu}>
      <div className="mx-auto max-w-7xl">
        <Form
          form={form}
          onFinish={onSubmit}
          // labelCol={{ span: 4 }}
          // wrapperCol={{ span: 25 }}
          layout="vertical"
          size="middle"
          style={{ maxWidth: 2000 }}
        >
          <Card bordered className="mt-3" bodyStyle={{ padding: "0" }}>
            <h1 className="font-semibold text-base flex justify-center mt-5">
              EVENT
            </h1>
            <Row>
              <Col lg={14} xs={24} className=" py-4 px-5">
                <div className="relative">
                  <Form.Item
                    style={{ marginBottom: "8px" }}
                    label="Нэр"
                    name="title"
                    rules={[
                      {
                        required: true,
                        message: "Нэр заавал оруулна уу!",
                      },
                    ]}
                    className="w-full"
                  >
                    <Input />
                  </Form.Item>
                </div>

                <Row className="" gutter={30}>
                  <Col sm={12} xs={24}>
                    <Form.Item
                      style={{ marginBottom: "8px" }}
                      label="Эхлэх өдөр"
                      name="startDate"
                      rules={[
                        {
                          required: true,
                          message: "Эхлэх өдөр сонгоно уу",
                        },
                      ]}
                    >
                      <DatePicker
                        disabled={id != undefined}
                        className="w-full"
                      />
                    </Form.Item>
                  </Col>
                  <Col sm={12} xs={24} className="flex items-center">
                    <Form.Item
                      style={{ marginBottom: "8px" }}
                      label="Дуусах өдөр"
                      name="endDate"
                      rules={[
                        {
                          required: true,
                          message: "Дуусах өдөр сонгоно уу",
                        },
                      ]}
                      className="w-full"
                    >
                      <DatePicker
                        disabled={id != undefined}
                        className="w-full"
                      />
                    </Form.Item>
                  </Col>
                </Row>

                <Row gutter={30} className="">
                  <Col xs={24} sm={12}>
                    <Form.Item
                      label="Эхлэх цаг"
                      name="startTime"
                      rules={[
                        {
                          required: true,
                          message: "Эхлэх цаг оруулна уу",
                        },
                      ]}
                      style={{ marginBottom: "8px" }}
                    >
                      <TimePicker
                        disabled={id != undefined}
                        className="w-full"
                        format={"HH:mm"}
                        minuteStep={15}
                        hourStep={1}
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12}>
                    <Form.Item
                      label="Дуусах цаг"
                      name="endTime"
                      style={{ marginBottom: "8px" }}
                      rules={[
                        {
                          required: true,
                          message: "Дуусах цаг оруулна уу",
                        },
                      ]}
                    >
                      <TimePicker
                        disabled={id != undefined}
                        className="w-full"
                        format={"HH:mm"}
                        minuteStep={10}
                        hourStep={1}
                      />
                    </Form.Item>
                  </Col>
                </Row>

                <Row gutter={30}>
                  <Col sm={12} xs={24}>
                    <Form.Item
                      label="Илтгэгчдийн тоо"
                      name="speaker"
                      style={{ marginBottom: "8px" }}
                      rules={[
                        {
                          required: true,
                          message: "Илтгэгчдийн тоо оруулна уу.",
                        },
                      ]}
                    >
                      <Input type="number" />
                    </Form.Item>
                  </Col>

                  <Col sm={12} xs={24}>
                    <Form.Item
                      label="Оролцох хүний тоо"
                      name="totalSlot"
                      style={{ marginBottom: "8px" }}
                      rules={[
                        {
                          required: true,
                          message: "Оролцох хүний тоо оруулна уу.",
                        },
                      ]}
                    >
                      <Input type="number" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col
                lg={10}
                xs={24}
                className={"py-4 px-5 border-l-0 lg:border-l-[1px]"}
              >
                <Form.Item
                  style={{ marginBottom: "8px" }}
                  label="Зураг"
                  name={"image"}
                  rules={[
                    {
                      required: true,
                      message: "Зураг оруулна уу",
                    },
                  ]}
                >
                  <Input placeholder="Зургын линк" type="text" />
                </Form.Item>
                <Form.Item
                  style={{ marginBottom: "8px" }}
                  label="Үнэ"
                  name="price"
                  rules={[
                    {
                      required: true,
                      message: "Үнэ оруулна уу",
                    },
                  ]}
                >
                  <Input type="number" placeholder="Үнэ:" />
                </Form.Item>
                <Form.Item
                  label="Дэлгэрэнгүй мэдээлэл"
                  name="content"
                  style={{ marginBottom: "8px" }}
                  rules={[
                    {
                      required: true,
                      message: "Дэлгэрэнгүй мэдээлэл оруулна уу.",
                    },
                  ]}
                >
                  <Input.TextArea
                    className="w-full mt-2"
                    autoSize={{
                      minRows: 4,
                      maxRows: 6,
                    }}
                    // style={{ width: 350 }}
                    rows={4}
                    placeholder="Дэлгэрэнгүй мэдээлэл:"
                  />
                </Form.Item>
                {/* <Form.Item
                  style={{ marginBottom: "8px" }}
                  label="Болох газар"
                  name="city"
                  rules={[
                    {
                      required: true,
                      message: "Болох газар сонгоно уу!",
                    },
                  ]}
                >
                  <Select
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      (option?.label.toString().toLowerCase() ?? "").includes(
                        input.toLowerCase()
                      )
                    }
                    // options={(props.countryList || []).map((d) => ({
                    //   value: d.pKey,
                    //   label: d.name,
                    // }))}
                  />
                </Form.Item> */}
                {/* <div style={{ height: "300px" }}>
                  <LocationSelector
                    defaultLocation={{ lat: 47.92, lng: 106.91 }}
                    onLocationSelect={handleLocationSelect}
                  />
                </div> */}
              </Col>
            </Row>
          </Card>
        </Form>
        <Button
          className="mt-5"
          onClick={() => {
            form.submit();
          }}
        >
          Хадгалах
        </Button>
      </div>
    </HomeLayout>
  );
};

export default CreateEvent;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session: Session | null = await getSession({ req: context.req });

  if (!session) {
    return {
      redirect: {
        destination: "/login", // Redirect to the login page if not logged in
        permanent: false,
      },
    };
  }
  const user = session?.user ?? null;
  const category = await prisma.category.findMany();

  return {
    props: {
      user: user,
      category,
    },
  };
};
