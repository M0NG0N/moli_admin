import { GetServerSidePropsContext, NextPage } from "next";

import React, { useState } from "react";
import {
  Button,
  Form,
  Input,
  Modal,
  Popconfirm,
  Select,
  Space,
  Table,
  Tag,
} from "antd";
import HomeLayout from "@/components/Layout";
import { Notification } from "@/components/notification";
import axios from "axios";
import { PrismaClient } from "@prisma/client";
import { Session } from "next-auth";
import { getSession } from "next-auth/react";
import StringToDate from "@/components/stringToDate";
import Link from "next/link";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
const prisma = new PrismaClient();

type TProps = {
  user: any;
  events: any;
};
interface User {
  id: string;
  name?: string | null | undefined;
  email?: string | null | undefined;
}

const EventList: NextPage<TProps> = ({ user, events }) => {
  const [data, setData] = useState(events);
  const [bookedTickets, setBookedTickets] = useState([]);
  const [open, setOpen] = useState(false);

  const deleteEvent = (id) => {
    axios
      .delete(`/api/event?id=${id}`)
      .then((res) => {
        if (res.data.success) {
          setData(data.filter((e) => e.id !== id));
          Notification("Амжилттай", "Амжилттай устгалдлаа!!!", "success");
        } else {
          Notification("Анхаар", "Илгээхэд алдаа гарсан!!!", "error");
        }
      })
      .catch((err) => console.log(err));
  };
  const getByUser = (id) => {
    axios
      .post(`/api/event/ticket`, { id })
      .then((res) => {
        if (res.data.success) {
          setBookedTickets(res.data.data);
          setOpen(true);
          // Notification("Амжилттай", "Амжилттай устгалдлаа!!!", "success");
        } else {
          Notification("Анхаар", "Илгээхэд алдаа гарсан!!!", "error");
        }
      })
      .catch((err) => console.log(err));
  };
  const columns = [
    {
      title: "№",
      key: "count",
      render: (text, object, index) => index + 1,
    },
    {
      key: "keyss",
      title: "Нэр",
      dataIndex: "title",
    },
    {
      key: "totalSlot",
      title: "Нийт тасалбар",
      dataIndex: "totalSlot",
    },
    {
      key: "totalBooked",
      title: "Худалдагдсан тасалбар",
      dataIndex: "totalBooked",
      render: (totalBooked, record) => (
        <Button onClick={() => getByUser(record.id)} type="dashed">
          {totalBooked}
        </Button>
      ),
    },
    {
      key: "createdDate",
      title: "Эхлэх огноо",
      dataIndex: "startDate",
      render: (createdDate) => (
        <div>
          <StringToDate stringDate={createdDate} format={"YYYY-MM-DD"} />
        </div>
      ),
    },
    {
      key: "modifiedDate",
      title: "Дуусах огноо",
      dataIndex: "endDate",
      render: (modifiedDate) => (
        <div>
          <StringToDate stringDate={modifiedDate} format={"YYYY-MM-DD"} />
        </div>
      ),
    },
    {
      title: "",
      key: "action",
      render: (text, record) => (
        <Space size="small" align="center">
          {record.vendorRequestId != 4 && (
            <Link
              href={{
                pathname: "/event",
                query: { id: record.id },
              }}
            >
              <EditOutlined className="text-[#319bec]" size={18} />
            </Link>
          )}
          <Popconfirm
            title="Та блог устгахдаа итгэлтэй байна уу?"
            onConfirm={() => {
              deleteEvent(record.id);
            }}
            okText={"Тийм"}
            cancelText="Үгүй"
          >
            <DeleteOutlined className="text-red-500" size={15} />
          </Popconfirm>
        </Space>
      ),
      width: 100,
    },
  ];
  const ticketColumns = [
    {
      title: "№",
      key: "count",
      render: (text, object, index) => index + 1,
    },
    {
      key: "firstName",
      title: "Нэр",
      dataIndex: "firstName",
    },
    {
      key: "email",
      title: "Email",
      dataIndex: "email",
    },
    {
      key: "totalQuantity",
      title: "Тасалбарын тоо",
      dataIndex: "totalQuantity",
    },
    {
      key: "totalPrice",
      title: "Нийт үнэ",
      dataIndex: "totalPrice",
    },
  ];
  return (
    <HomeLayout menu={user.menu}>
      <>
        <Table
          // loading={loading}
          bordered
          rowKey={"id"}
          className="myTable"
          locale={{ emptyText: "Өгөгдөл байхгүй байна" }}
          columns={columns}
          dataSource={data}
          pagination={{ size: "small", position: ["bottomCenter"] }}
        />
      </>
      <Modal
        open={open}
        title="Тасалбар"
        // closable={false}
        onCancel={() => setOpen(false)}
        footer={null}
      >
        <Table
          rowKey={"uid"}
          columns={ticketColumns}
          dataSource={bookedTickets}
          pagination={{ size: "small", position: ["bottomCenter"] }}
        />
      </Modal>
    </HomeLayout>
  );
};

export default EventList;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session: Session | null = await getSession({ req: context.req });

  if (!session) {
    return {
      redirect: {
        destination: "/login", // Redirect to the login page if not logged in
        permanent: false,
      },
    };
  }

  const tmpUser = session.user as User;

  const events = await prisma.events.findMany({
    where: {
      authorId: parseInt(tmpUser?.id),
    },
    select: {
      id: true,
      title: true,
      totalSlot: true,
      totalBooked: true,
      startDate: true,
      endDate: true,
    },
  });
  const serializedData = events.map((item) => ({
    ...item,
    startDate: item.startDate.toISOString(),
    endDate: item.endDate.toISOString(),
  }));
  const user = session?.user ?? null;

  return {
    props: {
      user: user,
      events: serializedData,
    },
  };
};
