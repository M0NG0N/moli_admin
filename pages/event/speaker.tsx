import { GetServerSidePropsContext, NextPage } from "next";

import React, { useState } from "react";
import { Button, Form, Input, Select, Tabs, Tag } from "antd";
import HomeLayout from "@/components/Layout";
import { Notification } from "@/components/notification";
import axios from "axios";
import { slugify } from "@/utils/slugify";
import dayjs from "dayjs";
import { CategoryType } from "@/utils/types";
import { PrismaClient } from "@prisma/client";
import { Session } from "next-auth";
import { getSession } from "next-auth/react";
const prisma = new PrismaClient();

type TProps = {
  user: any;
};

const tabs = [
  {
    label: `Илтгэгчийн жагсаалт `,
    key: "1",
    children: `Content of Tab Pane `,
  },
  {
    label: `Илтгэгч нэмэх `,
    key: "2",
    children: `Content of Tab Pane `,
  },
  {
    label: `Эвенттэй холбох `,
    key: "3",
    children: `Content of Tab Pane `,
  },
];
const Speaker: NextPage<TProps> = ({ user }) => {
  return (
    <HomeLayout menu={user.menu}>
      <Tabs type="card" items={tabs} />
    </HomeLayout>
  );
};

export default Speaker;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session: Session | null = await getSession({ req: context.req });

  if (!session) {
    return {
      redirect: {
        destination: "/login", // Redirect to the login page if not logged in
        permanent: false,
      },
    };
  }
  const user = session?.user ?? null;

  return {
    props: {
      user: user,
    },
  };
};
