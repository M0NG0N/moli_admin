export const convertToPassword = (password) => {
    password = password?.toString()
    if (!password || password.trim() === '') {
        return '';
    }

    let value = '';
    for (const char of password) {
        value += String.fromCharCode(char.charCodeAt(0) ^ 27);
    }

    return value;
}